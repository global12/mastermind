﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MasterMind
{
    public class GameManager : MonoBehaviour
    {
        public Action WinGame;
        public Action LoseGame;
        public Action NewGame;
        public Action<int, int> RoundResults;
        public Action<int> SelectedPegChanged;
        public Action<int> RoundChanged;
        public PegColor[] PegColors { get; private set; }
        public int SelectedPegColorIndex { get; private set; }
        public int Round  { get; private set; }
        public Code MasterMindCode { get; private set; }
        private BoardManager boardManager;
        private Code playerCode;
        private PegsDropdown pegsDropdown;
        private RowsDropdown rowsDropdown;

        private void Awake()
        {
            GetComponents();
            SetComponents();
        }

        private void GetComponents()
        {
            if (boardManager == null)
                boardManager = FindObjectOfType<BoardManager>();
            if (pegsDropdown == null)
                pegsDropdown = FindObjectOfType<PegsDropdown>();
            if (rowsDropdown == null)
                rowsDropdown = FindObjectOfType<RowsDropdown>();
        }

        private void SetComponents()
        {
            PegColors = new[]
            {
                new PegColor(0, "White", Color.white),
                new PegColor(1, "Gray", Color.gray),
                new PegColor(2, "Red", Color.red),
                new PegColor(3, "Yellow", Color.yellow),
                new PegColor(4, "Blue", Color.blue),
                new PegColor(5, "Green", Color.green),
                new PegColor(6, "Magento", Color.magenta),
                new PegColor(7, "Cyan", Color.cyan),
            };

            StartGame();
        }

        public void StartGame()
        {
            if (boardManager == null)
                return;

            Round = 0;
            CreateMasterMindCode();
            playerCode = new Code();
            boardManager.CreateBoard();
            NewGame?.Invoke();
        }

        private void CreateMasterMindCode()
        {
            if (pegsDropdown == null)
                return;

            int maxPossibilities = pegsDropdown.Pegs;
            int value1 = Random.Range(0, maxPossibilities);
            int value2 = Random.Range(0, maxPossibilities);
            int value3 = Random.Range(0, maxPossibilities);
            int value4 = Random.Range(0, maxPossibilities);
            MasterMindCode = new Code(value1, value2, value3, value4);
            Debug.Log($"MasterMindCode {PegColors[value1].Name} ({value1}) {PegColors[value2].Name} ({value2}) {PegColors[value3].Name} ({value3}) {PegColors[value4].Name} ({value4})");
        }

        public void PickPegFromPegBucket(int colorIndex)
        {
            SelectedPegColorIndex = colorIndex;
            SelectedPegChanged?.Invoke(colorIndex);
        }

        private bool IsCorrectCode()
        {
            return MasterMindCode.Value[0] == playerCode.Value[0]
                && MasterMindCode.Value[1] == playerCode.Value[1]
                && MasterMindCode.Value[2] == playerCode.Value[2]
                && MasterMindCode.Value[3] == playerCode.Value[3]
                ? true
                : false;
        }

        public void SetPegInSlot(int slotNumber, int code)
        {
            playerCode.Value[slotNumber] = code;
        }

        public void DoneRound()
        {
            if (rowsDropdown == null)
                return;

            CalculateRoundResults();

            if (IsCorrectCode() == false)
            {
                if (Round + 1 == rowsDropdown.Rows)
                {
                    LoseGame?.Invoke();
                }
                else
                {
                    Round++;
                    RoundChanged?.Invoke(Round);
                }
            }
            else
            {
                WinGame?.Invoke();
            }
        }

        private void CalculateRoundResults()
        {
            int rightPlace = 0;
            int colorOnOtherPlace = 0;
            bool[] matched = {false, false, false, false};
            bool[] used = {false, false, false, false};

            for (int i = 0; i < 4; i++)
            {
                if (playerCode.Value[i] == MasterMindCode.Value[i])
                {
                    rightPlace++;
                    matched[i] = true;
                    used[i] = true;
                }
            }

            for (int i = 0; i < 4; i++)
            {
                if (matched[i])
                    continue;

                for (int j = 0; j < 4; j++)
                {
                    if (i != j && !used[j] && playerCode.Value[i] == MasterMindCode.Value[j])
                    {
                        colorOnOtherPlace++;
                        used[j] = true;
                        break;
                    }
                }
            }

            RoundResults?.Invoke(rightPlace, colorOnOtherPlace);
        }
    }
}
