﻿using UnityEngine;
using UnityEngine.UI;

namespace MasterMind
{
    [RequireComponent(typeof(Image))]
    public class SmallPeg : MonoBehaviour
    {
        public void Init(Color color)
        {
            Image image = GetComponentInChildren<Image>();
            if (image != null)
                image.color = color;
        }
    }
}
