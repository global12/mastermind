﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MasterMind
{
    public class PegSlot : MonoBehaviour, IPointerClickHandler
    {
        public Action<int, int> SlotOccupied;
        public int RowNumber;
        public int SlotNumber;
        public bool IsOccupied;
        private Peg peg;
        private GameManager gameManager;

        private void Awake()
        {
            GetComponents();
        }

        private void GetComponents()
        {
            if (gameManager == null)
                gameManager = FindObjectOfType<GameManager>();
        }

        public void Init(int rowNumber, int slotNumber)
        {
            RowNumber = rowNumber;
            SlotNumber = slotNumber;
        }

        private void CreatePeg()
        {
            if (gameManager == null || gameManager.Round != RowNumber)
                return;

            if (peg == null)
            {
                InstantiatePeg();
            }
            else
            {
                peg.Destroy();
                InstantiatePeg();
            }

            IsOccupied = true;
            SlotOccupied?.Invoke(RowNumber, SlotNumber);
        }

        private void InstantiatePeg()
        {
            if (gameManager == null)
                return;

            peg = Instantiate(Resources.Load("Peg", typeof(Peg)), gameObject.transform) as Peg;
            if (peg != null)
            {
                peg.SetInSlot(RowNumber, SlotNumber, gameManager.SelectedPegColorIndex);
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            CreatePeg();
        }
    }
}
