﻿using UnityEngine;
using UnityEngine.UI;

namespace MasterMind
{
    public class MasterCode : MonoBehaviour
    {
        private GameManager gameManager;
        private Image[] pegSlots;

        private void Awake()
        {
            GetComponents();
            RegisterEvents();
            Disable();
        }

        private void OnEnable()
        {
            SetCode();
        }

        private void OnDestroy()
        {
            UnRegisterEvents();
        }

        private void OnApplicationQuit()
        {
            UnRegisterEvents();
        }

        private void GetComponents()
        {
            if (gameManager == null)
                gameManager = FindObjectOfType<GameManager>();
            if (pegSlots == null)
                pegSlots = GetComponentsInChildren<Image>();
        }

        private void SetCode()
        {
            if (gameManager == null)
                return;

            for (int i=0; i<pegSlots.Length; i++)
            {
                Peg peg = Instantiate(Resources.Load("Peg", typeof(Peg)), pegSlots[i].gameObject.transform) as Peg;
                if (peg != null)
                {
                    peg.SetColor(gameManager.MasterMindCode.Value[i]);
                }
            }
        }

        private void RegisterEvents()
        {
            if (gameManager != null)
            {
                gameManager.WinGame += WinGame_Handler;
                gameManager.LoseGame += LoseGame_Handler;
                gameManager.NewGame += NewGame_Handler;
            }
        }

        private void UnRegisterEvents()
        {
            if (gameManager != null)
            {
                gameManager.WinGame -= WinGame_Handler;
                gameManager.LoseGame -= LoseGame_Handler;
                gameManager.NewGame -= NewGame_Handler;
            }
        }

        private void Disable()
        {
            gameObject.SetActive(false);
        }

        private void Enable()
        {
            gameObject.SetActive(true);
        }

        private void OnWinGame()
        {
            Enable();
        }

        private void OnLoseGame()
        {
            Enable();
        }

        private void OnNewGame()
        {
            Disable();
        }

        private void WinGame_Handler()
        {
            OnWinGame();
        }

        private void LoseGame_Handler()
        {
            OnLoseGame();
        }

        private void NewGame_Handler()
        {
            OnNewGame();
        }
    }
}
