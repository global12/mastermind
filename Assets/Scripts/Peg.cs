using UnityEngine;
using UnityEngine.EventSystems;
using Image = UnityEngine.UI.Image;

namespace MasterMind
{
    [RequireComponent(typeof(Image))]
    public class Peg : MonoBehaviour, IPointerClickHandler
    {
        public int ColorIndex { get; private set; }
        public bool IsOnBoard  { get; set; }
        private GameManager gameManager;
        private int slotNumber;
        private int rowNumber;

        private void Awake()
        {
            GetComponents();
        }

        private void GetComponents()
        {
            if (gameManager == null)
                gameManager = FindObjectOfType<GameManager>();
        }

        public void SetColor(int colorIndex)
        {
            if (gameManager == null)
                return;

            Image image = GetComponentInChildren<Image>();
            if (image != null)
                image.color = gameManager.PegColors[colorIndex].Color;
            ColorIndex = colorIndex;
        }

        public void SetInSlot(int rowNumber, int slotNumber, int colorIndex)
        {
            if (gameManager == null)
                return;

            gameManager.SetPegInSlot(slotNumber, colorIndex);
            SetColor(colorIndex);
            IsOnBoard = true;
            this.slotNumber = slotNumber;
            this.rowNumber = rowNumber;
        }

        public void Destroy()
        {
            Destroy(gameObject);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (gameManager == null)
                return;

            if (IsOnBoard && gameManager.Round == rowNumber)
            {
                SetColor(gameManager.SelectedPegColorIndex);
                gameManager.SetPegInSlot(slotNumber, ColorIndex);
            }
            else if (!IsOnBoard)
                gameManager.PickPegFromPegBucket(ColorIndex);
        }
    }
}