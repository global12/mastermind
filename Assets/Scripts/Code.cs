﻿namespace MasterMind
{
    public class Code
    {
        public int[] Value { get; }

        public Code()
        {
            Value = new int[4];
        }

        public Code(int value1, int value2, int value3, int value4)
        {
            Value = new int[4];

            Value[0] = value1;
            Value[1] = value2;
            Value[2] = value3;
            Value[3] = value4;
        }
    }
}
