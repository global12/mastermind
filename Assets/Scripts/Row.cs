﻿using System.Collections.Generic;
using UnityEngine;

namespace MasterMind
{
    public class Row : MonoBehaviour
    {
        public int RowNumber;
        private PegSlotContainer pegSlotContainer;
        private List<PegSlot> pegSlots;
        private DoneButton doneButton;
        private ResultPanel resultPanel;

        private void Awake()
        {
            GetComponents();
            SetComponents();
        }

        private void OnDestroy()
        {
            UnRegisterEvents();
        }

        private void OnApplicationQuit()
        {
            UnRegisterEvents();
        }

        private void GetComponents()
        {
            if (pegSlotContainer == null)
                pegSlotContainer = FindObjectOfType<PegSlotContainer>();
            if (doneButton == null)
                doneButton = GetComponentInChildren<DoneButton>();
            if (resultPanel == null)
                resultPanel = GetComponentInChildren<ResultPanel>();
        }

        private void SetComponents()
        {
            pegSlots = new List<PegSlot>();
        }

        private void UnRegisterEvents()
        {
            if (pegSlots == null)
                return;

            if (pegSlots != null && pegSlots.Count > 0)
            {
                foreach (PegSlot slot in pegSlots)
                {
                    slot.SlotOccupied -= SlotOccupied_Handler;
                }
            }
        }

        public void Init(int rowNumber)
        {
            RowNumber = rowNumber;

            if (doneButton != null)
                doneButton.Init(rowNumber);

            if (resultPanel != null)
                resultPanel.Init(rowNumber);

            CreatePegSlots();
        }

        private void CreatePegSlots()
        {
            if (pegSlotContainer == null)
                return;

            for (int i = 0; i < 4; i++)
            {
                PegSlot pegSlot = Instantiate(Resources.Load("PegSlot", typeof(PegSlot)), pegSlotContainer.transform) as PegSlot;
                if (pegSlot != null)
                {
                    pegSlot.Init(RowNumber, i);
                    pegSlot.SlotOccupied += SlotOccupied_Handler;
                    pegSlots.Add(pegSlot);
                }
            }
        }

        public void Destroy()
        {
            Destroy(gameObject);
        }

        private void CheckAllSlotsAreOccupied()
        {
            if (doneButton == null || pegSlots == null)
                return;

            foreach (PegSlot slot in pegSlots)
            {
                if (!slot.IsOccupied)
                    return;
            }

            doneButton.SetInteractable();
        }

        private void OnSlotOccupied(int rowNumber, int slotNumber)
        {
            CheckAllSlotsAreOccupied();
        }

        private void SlotOccupied_Handler(int rowNumber, int slotNumber)
        {
            OnSlotOccupied(rowNumber, slotNumber);
        }
    }
}
