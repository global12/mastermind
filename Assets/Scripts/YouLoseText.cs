﻿using UnityEngine;

namespace MasterMind
{
    public class YouLoseText : MonoBehaviour
    {
        private GameManager gameManager;

        private void Awake()
        {
            GetComponents();
            RegisterEvents();
            Disable();
        }

        private void OnDestroy()
        {
            UnRegisterEvents();
        }

        private void OnApplicationQuit()
        {
            UnRegisterEvents();
        }

        private void GetComponents()
        {
            if (gameManager == null)
                gameManager = FindObjectOfType<GameManager>();
        }

        private void RegisterEvents()
        {
            if (gameManager != null)
            {
                gameManager.LoseGame += LoseGame_Handler;
                gameManager.NewGame += NewGame_Handler;
            }
        }

        private void UnRegisterEvents()
        {
            if (gameManager != null)
            {
                gameManager.LoseGame -= LoseGame_Handler;
                gameManager.NewGame -= NewGame_Handler;
            }
        }

        private void Disable()
        {
            gameObject.SetActive(false);
        }

        private void Enable()
        {
            gameObject.SetActive(true);
        }

        private void OnLoseGame()
        {
            Enable();
        }

        private void OnNewGame()
        {
            Disable();
        }

        private void LoseGame_Handler()
        {
            OnLoseGame();
        }

        private void NewGame_Handler()
        {
            OnNewGame();
        }
    }
}
