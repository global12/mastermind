﻿using UnityEngine;

namespace MasterMind
{
    public class ResultPanel : MonoBehaviour
    {
        public int RowNumber;
        private GameManager gameManager;

        private void Awake()
        {
            GetComponents();
            RegisterEvents();
        }

        private void OnDestroy()
        {
            UnRegisterEvents();
        }

        private void OnApplicationQuit()
        {
            UnRegisterEvents();
        }

        private void GetComponents()
        {
            gameManager = FindObjectOfType<GameManager>();
        }

        private void RegisterEvents()
        {
            if (gameManager != null)
                gameManager.RoundResults += RoundResults_Handler;
        }

        private void UnRegisterEvents()
        {
            if (gameManager != null)
                gameManager.RoundResults -= RoundResults_Handler;
        }

        public void Init(int rowNumber)
        {
            RowNumber = rowNumber;
        }

        private void CreatePegSlots(Color color)
        {
            SmallPeg smallPeg = Instantiate(Resources.Load("SmallPeg", typeof(SmallPeg)), transform) as SmallPeg;
            if (smallPeg != null)
                smallPeg.Init(color);
        }

        private void OnRoundResults(int rightPlace, int colorOnOtherPlace)
        {
            if (gameManager == null || gameManager.Round != RowNumber)
                return;

            for (int i = 0; i < rightPlace; i++)
                CreatePegSlots(new Color(0.08f,0.08f,0.08f));
            for (int i = 0; i < colorOnOtherPlace; i++)
                CreatePegSlots(new Color(1f,1f,1f));
        }

        private void RoundResults_Handler(int rightPlace, int colorOnOtherPlace)
        {
            OnRoundResults(rightPlace, colorOnOtherPlace);
        }
    }
}
