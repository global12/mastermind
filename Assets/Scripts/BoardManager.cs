﻿using System.Collections.Generic;
using UnityEngine;

namespace MasterMind
{
    public class BoardManager : MonoBehaviour
    {
        private Column column;
        private PegBucket pegBucket;
        private List<Row> rows;
        private List<Peg> availablePegs;
        private PegsDropdown pegsDropdown;
        private RowsDropdown rowsDropdown;
        private GameManager gameManager;

        private void Awake()
        {
            GetComponents();
            SetComponents();
        }

        private void GetComponents()
        {
            if (column == null)
                column = FindObjectOfType<Column>();
            if (pegBucket == null)
                pegBucket = FindObjectOfType<PegBucket>();
            if (gameManager == null)
                gameManager = FindObjectOfType<GameManager>();
        }

        private void SetComponents()
        {
            rows = new List<Row>();
            availablePegs = new List<Peg>();
            CreateBoard();
        }

        public void CreateBoard()
        {
            CreateAvailablePegs();
            CreateRows();
        }

        private void CreateAvailablePegs()
        {
            pegsDropdown = FindObjectOfType<PegsDropdown>();

            if (column == null || pegsDropdown == null)
                return;

            DestroyAvailablePegs();

            for (int i = 0; i < pegsDropdown.Pegs; i++)
            {
                Peg peg = Instantiate(Resources.Load("Peg", typeof(Peg)), pegBucket.transform) as Peg;
                if (peg != null)
                {
                    peg.SetColor(i);
                    availablePegs.Add(peg);
                }
            }
        }

        private void DestroyAvailablePegs()
        {
            foreach (Peg peg in availablePegs)
                peg.Destroy();

            availablePegs.Clear();
        }

        private void CreateRows()
        {
            rowsDropdown = FindObjectOfType<RowsDropdown>();

            if (column == null || rowsDropdown == null)
                return;

            DestroyRows();

            for (int i = rowsDropdown.Rows-1; i >= 0; i--)
            {
                Row row = Instantiate(Resources.Load("Row", typeof(Row)), column.transform) as Row;
                if (row != null)
                {
                    row.Init(i);
                    rows.Add(row);
                }
            }
        }

        private void DestroyRows()
        {
            if (rows == null)
                return;

            foreach (Row row in rows)
                row.Destroy();

            rows.Clear();
        }
    }
}
