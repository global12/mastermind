﻿using UnityEngine;

namespace MasterMind
{
    public class PegColor
    {
        public int Index { get; private set; }
        public string Name { get; private set; }
        public Color Color { get; private set; }

        public PegColor(int index, string name, Color color)
        {
            Index = index;
            Name = name;
            Color = color;
        }
    }
}
