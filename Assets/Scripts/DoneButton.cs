﻿using UnityEngine;
using UnityEngine.UI;

namespace MasterMind
{
    [RequireComponent(typeof(Button))]
    public class DoneButton : MonoBehaviour
    {
        public int RowNumber;
        private Button button;
        private GameManager gameManager;

        private void Awake()
        {
            GetComponents();
            SetComponents();
            RegisterEvents();
        }

        private void OnDestroy()
        {
            UnRegisterEvents();
        }

        private void OnApplicationQuit()
        {
            UnRegisterEvents();
        }

        private void GetComponents()
        {
            if (button == null)
                button = GetComponent<Button>();
            if (gameManager == null)
                gameManager = FindObjectOfType<GameManager>();
        }

        private void SetComponents()
        {
            if (button != null)
                button.interactable = false;
        }

        private void RegisterEvents()
        {
            if (button != null)
                button.onClick.AddListener(DoneButton_Handler);
            if (gameManager != null)
                gameManager.RoundChanged += RoundChanged_Handler;
        }

        private void UnRegisterEvents()
        {
            if (button != null)
                button.onClick.RemoveAllListeners();
            if (gameManager != null)
                gameManager.RoundChanged -= RoundChanged_Handler;
        }

        private void Disable()
        {
            gameObject.SetActive(false);
        }

        private void Enable()
        {
            gameObject.SetActive(true);
        }

        public void Init(int rowNumber)
        {
            RowNumber = rowNumber;

            if (rowNumber == 0)
                Enable();
            else
                Disable();
        }

        public void SetInteractable()
        {
            if (button != null)
                button.interactable = true;
        }

        private void OnDoneButton()
        {
            gameManager.DoneRound();
            Disable();
        }

        private void OnRoundChanged(int round)
        {
            if (round != RowNumber)
                Disable();
            else
                Enable();
        }

        private void DoneButton_Handler()
        {
            OnDoneButton();
        }

        private void RoundChanged_Handler(int round)
        {
            OnRoundChanged(round);
        }
    }
}
