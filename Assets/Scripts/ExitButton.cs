﻿using UnityEngine;
using UnityEngine.UI;

namespace MasterMind
{
    [RequireComponent(typeof(Button))]
    public class ExitButton : MonoBehaviour
    {
        private Button button;

        private void Awake()
        {
            GetComponents();
            RegisterEvents();
        }

        private void OnDestroy()
        {
            UnRegisterEvents();
        }

        private void OnApplicationQuit()
        {
            UnRegisterEvents();
        }

        private void GetComponents()
        {
            if (button == null)
                button = GetComponent<Button>();
        }

        private void RegisterEvents()
        {
            if (button != null)
                button.onClick.AddListener(ExitButton_Handler);
        }

        private void UnRegisterEvents()
        {
            if (button != null)
                button.onClick.RemoveAllListeners();
        }

        private void OnExitButton()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
        }

        private void ExitButton_Handler()
        {
            OnExitButton();
        }
    }
}
