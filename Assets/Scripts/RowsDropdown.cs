﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MasterMind
{
    [RequireComponent(typeof(Dropdown))]
    public class RowsDropdown : MonoBehaviour
    {
        private const int DEFAULT_ROWS = 10;

        public int Rows { get; private set; }

        private Dropdown rowsDropdown;

        private void Awake()
        {
            GetComponents();
            SetComponents();
            RegisterEvents();
        }

        private void OnDestroy()
        {
            UnRegisterEvents();
        }

        private void OnApplicationQuit()
        {
            UnRegisterEvents();
        }

        private void GetComponents()
        {
            if (rowsDropdown == null)
                rowsDropdown = GetComponent<Dropdown>();
        }

        private void SetComponents()
        {
            if (rowsDropdown != null)
            {
                List<string> options = new List<string>();
                options.Add("8");
                options.Add("10");
                options.Add("12");

                rowsDropdown.ClearOptions();
                rowsDropdown.AddOptions(options);
                rowsDropdown.value = 1;
            }

            Rows = DEFAULT_ROWS;
        }

        private void RegisterEvents()
        {
            if (rowsDropdown != null)
                rowsDropdown.onValueChanged.AddListener(ValueChanged_Handler);
        }

        private void UnRegisterEvents()
        {
            if (rowsDropdown != null)
                rowsDropdown.onValueChanged.RemoveAllListeners();
        }

        private void OnValueChanged_Handler(int index)
        {
            Rows = int.Parse(rowsDropdown.options[index].text);
        }

        private void ValueChanged_Handler(int index)
        {
            OnValueChanged_Handler(index);
        }
    }
}
