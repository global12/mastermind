﻿using UnityEngine;

namespace MasterMind
{
    public class MasterMindText : MonoBehaviour
    {
        private GameManager gameManager;

        private void Awake()
        {
            GetComponents();
            RegisterEvents();
            Enable();
        }

        private void OnDestroy()
        {
            UnRegisterEvents();
        }

        private void OnApplicationQuit()
        {
            UnRegisterEvents();
        }

        private void GetComponents()
        {
            if (gameManager == null)
                gameManager = FindObjectOfType<GameManager>();
        }

        private void RegisterEvents()
        {
            if (gameManager != null)
            {
                gameManager.WinGame += WinGame_Handler;
                gameManager.LoseGame += LoseGame_Handler;
                gameManager.NewGame += NewGame_Handler;
            }
        }

        private void UnRegisterEvents()
        {
            if (gameManager != null)
            {
                gameManager.WinGame -= WinGame_Handler;
                gameManager.LoseGame -= LoseGame_Handler;
                gameManager.NewGame -= NewGame_Handler;
            }
        }

        private void Disable()
        {
            gameObject.SetActive(false);
        }

        private void Enable()
        {
            gameObject.SetActive(true);
        }

        private void OnWinGame()
        {
            Disable();
        }

        private void OnLoseGame()
        {
            Disable();
        }

        private void OnNewGame()
        {
            Enable();
        }

        private void WinGame_Handler()
        {
            OnWinGame();
        }

        private void LoseGame_Handler()
        {
            OnLoseGame();
        }

        private void NewGame_Handler()
        {
            OnNewGame();
        }
    }
}
