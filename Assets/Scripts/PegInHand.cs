﻿using UnityEngine;
using UnityEngine.UI;

namespace MasterMind
{
    public class PegInHand : MonoBehaviour
    {
        private GameManager gameManager;
        private Image image;

        private void Awake()
        {
            GetComponents();
            RegisterEvents();
        }

        private void OnDestroy()
        {
            UnRegisterEvents();
        }

        private void OnApplicationQuit()
        {
            UnRegisterEvents();
        }

        private void GetComponents()
        {
            if (gameManager == null)
                gameManager = FindObjectOfType<GameManager>();
            if (image == null)
                image = GetComponentInChildren<Image>();
        }

        private void RegisterEvents()
        {
            if (gameManager != null)
                gameManager.SelectedPegChanged += SelectedPegChanged_Handler;
        }

        private void UnRegisterEvents()
        {
            if (gameManager != null)
                gameManager.SelectedPegChanged -= SelectedPegChanged_Handler;
        }

        private void Disable()
        {
            gameObject.SetActive(false);
        }

        private void Enable()
        {
            gameObject.SetActive(true);
        }

        private void SetColorPeg(int colorIndex)
        {
            if (image == null)
                return;

            image.color = gameManager.PegColors[colorIndex].Color;
        }

        private void OnSelectedPegChanged(int colorIndex)
        {
            Enable();
            SetColorPeg(colorIndex);
        }

        private void SelectedPegChanged_Handler(int colorIndex)
        {
            OnSelectedPegChanged(colorIndex);
        }
    }
}
