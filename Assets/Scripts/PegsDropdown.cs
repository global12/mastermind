﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MasterMind
{
    [RequireComponent(typeof(Dropdown))]
    public class PegsDropdown : MonoBehaviour
    {
        private const int DEFAULT_PEGS = 6;

        public int Pegs { get; private set; }

        private Dropdown pegsDropdown;

        private void Awake()
        {
            GetComponents();
            SetComponents();
            RegisterEvents();
        }

        private void OnDestroy()
        {
            UnRegisterEvents();
        }

        private void OnApplicationQuit()
        {
            UnRegisterEvents();
        }

        private void GetComponents()
        {
            if (pegsDropdown == null)
                pegsDropdown = GetComponent<Dropdown>();
        }

        private void SetComponents()
        {
            if (pegsDropdown != null)
            {
                List<string> options = new List<string>();
                options.Add("6");
                options.Add("7");
                options.Add("8");

                pegsDropdown.ClearOptions();
                pegsDropdown.AddOptions(options);
                pegsDropdown.value = 0;
            }

            Pegs = DEFAULT_PEGS;
        }

        private void RegisterEvents()
        {
            if (pegsDropdown != null)
                pegsDropdown.onValueChanged.AddListener(ValueChanged_Handler);
        }

        private void UnRegisterEvents()
        {
            if (pegsDropdown != null)
                pegsDropdown.onValueChanged.RemoveAllListeners();
        }

        private void OnValueChanged_Handler(int index)
        {
            Pegs = int.Parse(pegsDropdown.options[index].text);
        }

        private void ValueChanged_Handler(int index)
        {
            OnValueChanged_Handler(index);
        }
    }
}
