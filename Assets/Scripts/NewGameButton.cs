﻿using UnityEngine;
using UnityEngine.UI;

namespace MasterMind
{
    [RequireComponent(typeof(Button))]
    public class NewGameButton : MonoBehaviour
    {
        private Button button;
        private GameManager gameManager;

        private void Awake()
        {
            GetComponents();
            RegisterEvents();
        }

        private void OnDestroy()
        {
            UnRegisterEvents();
        }

        private void OnApplicationQuit()
        {
            UnRegisterEvents();
        }

        private void GetComponents()
        {
            if (button == null)
                button = GetComponent<Button>();
            if (gameManager == null)
                gameManager = FindObjectOfType<GameManager>();
        }

        private void RegisterEvents()
        {
            if (button != null)
                button.onClick.AddListener(StartButton_Handler);
        }

        private void UnRegisterEvents()
        {
            if (button != null)
                button.onClick.RemoveAllListeners();
        }

        private void OnStartButton()
        {
            if (gameManager == null)
                return;

            gameManager.StartGame();
        }

        private void StartButton_Handler()
        {
            OnStartButton();
        }
    }
}
